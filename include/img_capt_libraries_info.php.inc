<?php

/**
 * Implements hook_library_info().
 */
function img_capt_libraries_info() {
  $libraries['simple_html_dom'] = array(
    'name' => 'PHP Simple HTML DOM Parser',
    'vendor url' => 'http://simplehtmldom.sourceforge.net/',
    'download url' => 'http://sourceforge.net/projects/simplehtmldom/files/',
    'version' => '1.5',
    'version arguments' => array(
      'file' => 'simple_html_dom.php',
      'pattern' => '|version\s([0-9]\.0-9])|',
      'lines' => 1500,
    ),
    'files' => array(
      // For PHP libraries, specify include files here, still relative to the
      // library path.
      'php' => array(
        'simple_html_dom.php',
      ),
    ),
  );

  return $libraries;
}
