<?php

/**
 * Implements hook_node_view().
 *
 * This one is for Drupal with NO Panels/Panels Everywhere
 */
function img_capt_node_view($node, $view_mode, $langcode) {
  // $settings = unserialize(base64_decode(variable_get('img_capt_settings')));
  $settings = img_capt_get_sanetized_settings();

  foreach ($settings as $setting) {
    if ($setting['content_type'] == $node->type) {
      if (isset($node->content['body'][0]['#markup'])) {
        $doc = $node->content['body'][0]['#markup'];
        $node->content['body'][0]['#markup'] = img_capt_image_caption($doc, $node->type);
      }
      break;
    }
  }
}

