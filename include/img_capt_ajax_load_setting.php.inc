<?php

/**
 * Load setting through ajax and return a settings.
 */
function img_capt_ajax_load_setting($form, $form_state) {
  if (!user_access('administer img_capt')) {
    drupal_access_denied();
    drupal_exit();
  }

  // Use this var to prevent DrupalPractice WARNINGs.
  $form_state_key = "input";

  // One of "New setting", "Remove setting", "Save setting" buttons are clicked
  // handle it with validation.
  if (isset($form_state['values']['op'])) {
    img_capt_admin_validate($form, $form_state);
    return;
  }

  $path = drupal_get_path('module', 'img_capt');
  drupal_add_css($path . '/css/admin-page.css');
  drupal_add_css($path . '/css/caption.css');

  if (isset($form_state['values']['selected_setting'])) {
    $loaded_setting = $form_state['values']['selected_setting'];
  }
  else {
    $loaded_setting = '0';
  }
  if ($loaded_setting == '0') {
    $loaded_setting = 'Default';
  }

  // $settings = unserialize(base64_decode(variable_get('img_capt_settings')));
  $settings = img_capt_get_sanetized_settings();
  $setting = $settings[$loaded_setting];

  // Load defined settings.
  $opts = array(t('Default'));
  $weights  = '<table id="weights_markup">';
  $weights .= '<tr>';
  $weights .= '<th>' . t('Name') . '</th>';
  $weights .= '<th>' . t('Weight') . '</th>';
  $weights .= '<th>' . t('Path') . '</th>';
  $weights .= '<th>' . t('Content type') . '</th>';
  $weights .= '<th>' . t('Panel title') . '</th>';
  $weights .= '</tr>';
  foreach ($settings as $key => $value) {
    if ($key != 'Default') {
      $opts[$key] = $key;
    }
    $weights .= '<tr>';
    $weights .= '<td>' . $key . '</td>';
    $weights .= '<td>';
    $weights .= isset($value['weight']) ? $value['weight'] : '---';
    $weights .= '</td>';
    $weights .= '<td>' . $value['content_path'] . '</td>';
    $weights .= '<td>' . $value['content_type'] . '</td>';
    $weights .= '<td>' . $value['pane_admin_title'] . '</td>';
    $weights .= '</tr>';
  }
  $weights .= '</table>';

  // Load content types.
  $content_types = array();
  $c_type = node_type_get_types();
  foreach ($c_type as $key => $value) {
    $content_types[$key] = $c_type[$key]->name;
  }
  $content_types['custom'] = t('Custom');

  $form['form_settings'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'clearfix',
      ),
      'id' => 'loaded-setting',
    ),
  );
  $form['form_settings']['selected_setting'] = array(
    '#title' => t('Active settings'),
    '#description' => t('Settings that are defined and active. Choose one to load.'),
    '#type' => 'select',
    '#options' => $opts,
    '#default_value' => '0',
    '#ajax' => array(
      'callback' => 'img_capt_ajax_load_setting_callback',
      'wrapper' => 'loaded_setting_form',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['form_settings']['settings'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'clearfix',
      ),
      'id' => 'loaded_setting_form',
    ),
  );
  $form['form_settings']['settings']['loaded_setting'] = array(
    '#type' => 'fieldset',
    '#title' => $loaded_setting,
    '#collapsible' => TRUE,
  );
  $form['form_settings']['settings']['loaded_setting']['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $loaded_setting,
    '#size' => 20,
    '#maxlenght' => 20,
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
  );
  unset($form_state[$form_state_key]['name']);
  $form_state['values']['name'] = $loaded_setting;

  $form['form_settings']['settings']['loaded_setting']['content_path'] = array(
    '#title' => t('Content path'),
    '#type' => 'textfield',
    '#default_value' => $setting['content_path'],
    '#size' => 20,
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
    '#suffix' => '<div class="clearfix"></div>',
  );
  unset($form_state[$form_state_key]['content_path']);
  $form_state['values']['content_path'] = $setting['content_path'];

  $form['form_settings']['settings']['loaded_setting']['pane_admin_title'] = array(
    '#title' => t('Pane admin title<br/>or machine name'),
    '#type' => 'textfield',
    '#default_value' => $setting['pane_admin_title'],
    '#size' => 20,
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
  );
  unset($form_state[$form_state_key]['pane_admin_title']);
  $form_state['values']['pane_admin_title'] = $setting['pane_admin_title'];

  $form['form_settings']['settings']['loaded_setting']['div_pre_style'] = array(
    '#title' => t('<br/>Wrapper style'),
    '#type' => 'textfield',
    '#default_value' => $setting['div_pre_style'],
    '#size' => 20,
    '#maxlenght' => 20,
    '#required' => FALSE,
    '#disabled' => ($loaded_setting == 'Default'),
    '#suffix' => '<div class="clearfix"></div>',
  );
  unset($form_state[$form_state_key]['div_pre_style']);
  $form_state['values']['div_pre_style'] = $setting['div_pre_style'];

  $form['form_settings']['settings']['loaded_setting']['cap_tag'] = array(
    '#title' => t('Caption tag'),
    '#type' => 'select',
    '#default_value' => 'H6',
    '#options' => array(
      'DIV' => t('DIV'),
      'SPAN' => t('SPAN'),
      'H3' => t('H3'),
      'H4' => t('H4'),
      'H5' => t('H5'),
      'H6' => t('H6'),
    ),
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
  );
  unset($form_state[$form_state_key]['cap_tag']);
  $form_state['values']['cap_tag'] = strtoupper($setting['cap_tag']);

  $form['form_settings']['settings']['loaded_setting']['cap_tag_style'] = array(
    '#title' => t('Caption style'),
    '#type' => 'textfield',
    '#default_value' => $setting['cap_tag_style'],
    '#size' => 20,
    '#maxlenght' => 20,
    '#required' => FALSE,
    '#disabled' => ($loaded_setting == 'Default'),
    '#suffix' => '<div class="clearfix"></div>',
  );
  unset($form_state[$form_state_key]['cap_tag_style']);
  $form_state['values']['cap_tag_style'] = $setting['cap_tag_style'];

  $form['form_settings']['settings']['loaded_setting']['def_caption'] = array(
    '#title' => t('Caption text'),
    '#type' => 'textfield',
    '#default_value' => $setting['def_caption'],
    '#size' => 20,
    '#maxlenght' => 20,
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
  );
  unset($form_state[$form_state_key]['def_caption']);
  $form_state['values']['def_caption'] = $setting['def_caption'];

  $form['form_settings']['settings']['loaded_setting']['caption_count'] = array(
    '#title' => t('Caption start no.'),
    '#type' => 'textfield',
    '#default_value' => strtoupper($setting['caption_count']),
    '#size' => 20,
    '#maxlenght' => 20,
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
    '#suffix' => '<div class="clearfix"></div>',
  );
  unset($form_state[$form_state_key]['caption_count']);
  $form_state['values']['caption_count'] = strtoupper($setting['caption_count']);

  $form['form_settings']['settings']['loaded_setting']['caption_markup'] = array(
    '#title' => t('Caption attribute'),
    '#type' => 'select',
    '#default_value' => t('ALT'),
    '#options' => array('TITLE' => t('TITLE'), 'ALT' => t('ALT')),
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
  );
  unset($form_state[$form_state_key]['caption_markup']);
  $form_state['values']['caption_markup'] = strtoupper($setting['caption_markup']);

  $form['form_settings']['settings']['loaded_setting']['content_type'] = array(
    '#title' => t('Content type'),
    '#type' => 'select',
    '#default_value' => $setting['content_type'],
    '#options' => $content_types,
    '#required' => TRUE,
    '#disabled' => ($loaded_setting == 'Default'),
    '#suffix' => '</td><div class="clearfix"></div>',
  );
  $form['form_settings']['settings']['loaded_setting']['weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $setting['weight'],
    '#disabled' => ($loaded_setting == 'Default'),
  );

  $form['form_settings']['overview'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'loaded_setting_form_weight',
    ),
  );
  $form['form_settings']['overview']['loaded_setting_weight'] = array(
    '#type' => 'fieldset',
    '#title' => t('Overview'),
    '#collapsible' => TRUE,
  );
  $form['form_settings']['overview']['loaded_setting_weight']['weight'] = array(
    '#title' => t('Weight'),
    '#markup' => $weights,
  );

  unset($form_state[$form_state_key]['content_type']);
  $form_state['values']['content_type'] = $setting['content_type'];

  $form['submit_buttons'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'loaded_setting_buttons',
    ),
  );
  $form['submit_buttons']['new_setting'] = array(
    '#type' => 'button',
    '#value' => t('New setting'),
  );
  $form['submit_buttons']['remove_setting'] = array(
    '#type' => 'button',
    '#value' => t('Remove setting'),
  );
  $form['submit_buttons']['save_setting'] = array(
    '#type' => 'button',
    '#value' => t('Save setting'),
  );

  return $form;
}


