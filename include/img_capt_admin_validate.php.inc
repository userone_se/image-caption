<?php

/**
 * Implements hook_admin_validate().
 */
function img_capt_admin_validate($form, $form_state) {
  if (!user_access('administer img_capt')) {
    drupal_access_denied();
    drupal_exit();
  }

  // $settings = unserialize(base64_decode(variable_get('img_capt_settings')));
  $settings = img_capt_get_sanetized_settings();

  // "Save setting" klicked.
  if ($form_state['values']['op'] == t('Save setting')) {
    // Check if it exists - no rename, first delete.
    $setting_name = $form_state['values']['selected_setting'];
    if (array_key_exists($setting_name, $settings)) {
      unset($settings[$setting_name]);
    }

    // Then save the new value.
    $settings[$form_state['values']['name']] = array(
      'content_path'      => $form_state['values']['content_path'],
      'pane_admin_title'  => $form_state['values']['pane_admin_title'],
      'div_pre'           => strtolower('<div class="img-capt-caption-wrapper"'),
      'div_pre_style'     => strtolower($form_state['values']['div_pre_style']),
      'div_aft'           => strtolower('</div>'),
      'cap_tag'           => strtolower($form_state['values']['cap_tag']),
      'cap_tag_style'     => strtolower($form_state['values']['cap_tag_style']),
      'def_caption'       => $form_state['values']['def_caption'],
      'caption_markup'    => strtolower($form_state['values']['caption_markup']),
      'caption_count'     => $form_state['values']['caption_count'],
      'content_type'      => $form_state['values']['content_type'],
      'weight'            => $form_state['values']['weight'],
    );

    // Sort by weight.
    $weighted = array();
    foreach ($settings as $set_name => $setting) {
      $weighted[$setting['weight']][] = $set_name;
    }
    ksort($weighted);
    $new_settings = array();
    foreach ($weighted as $value) {
      foreach ($value as $v) {
        $new_settings[$v] = $settings[$v];
      }
    }

    variable_set('img_capt_settings', base64_encode(serialize($new_settings)));
    drupal_set_message(t('Setting "@form_name" is saved.', array('@form_name' => $form_state['values']['name'])), 'status');
  }

  // "Remove setting" klicked.
  if ($form_state['values']['op'] == t('Remove setting')) {
    // Check if it exists, delete.
    if (array_key_exists($form_state['values']['selected_setting'], $settings)) {
      $delete_setting = $form_state['values']['selected_setting'];
      if ($delete_setting != 'Default') {
        unset($settings[$delete_setting]);
        variable_set('img_capt_settings', base64_encode(serialize($settings)));
        drupal_set_message(t('Setting "@form_setting" deleted.', array('@form_setting' => $delete_setting)), 'status');
      }
      else {
        drupal_set_message(t('Can not delete "Default"!'), 'error');
      }
    }
  }

  // "New setting" klicked.
  if ($form_state['values']['op'] == t('New setting')) {
    // Create empty set.
    $alphas = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz';
    for ($i = 0; $i < 5; $i++) {
      $rand_alphas = str_shuffle($alphas);
    }
    $setting_name = substr($rand_alphas, 0, 10);
    $settings[$setting_name] = $settings['Default'];
    // Find max weight.
    $max_weight = -1000;
    foreach ($settings as $value) {
      if ($value['weight'] > $max_weight) {
        $max_weight = $value['weight'];
      }
    }
    $settings[$setting_name]['weight'] = ++$max_weight;
    variable_set('img_capt_settings', base64_encode(serialize($settings)));
    drupal_set_message(t('Your new setting is called (only letters): @new_setting', array('@new_setting' => $setting_name)), 'status');
  }

  drupal_goto('admin/config/media/img_capt/admin');
  drupal_exit();
}

