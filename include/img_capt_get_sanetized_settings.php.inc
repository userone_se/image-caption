<?php

/**
 * Get (sanetized) settings.
 *
 * @param bool $is_drush
 *   Set by drush to avoid going to the admin page.
 */
function img_capt_get_sanetized_settings($is_drush = FALSE) {
  $var_settings = filter_xss(variable_get('img_capt_settings'));
  if (drupal_validate_utf8($var_settings) == TRUE) {
    $settings = unserialize(base64_decode(check_plain($var_settings)));
    return $settings;
  }
  drupal_set_message(t('Currupt settings, uninstall and reinstall module.'), 'error');
  if ($is_drush == FALSE) {
    drupal_goto('admin/config/media/img_capt/admin');
  }
  drupal_exit();
}

