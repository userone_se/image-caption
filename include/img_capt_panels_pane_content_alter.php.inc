<?php

/**
 * Implements hook_panels_pane_content_alter().
 *
 * This one is for Drupal with Panels/Panes Everywhere.
 */
function img_capt_panels_pane_content_alter($content, $pane, $args, $contexts) {
  // $settings = unserialize(base64_decode(variable_get('img_capt_settings')));
  $settings = img_capt_get_sanetized_settings();

  // Using "Node body" in category "Node" of panels add content to pane.
  // dpm($pane, $pane->panel . " | " . $pane->type . " | " . $pane->subtype);

  switch ($pane->type) {
    case 'node_body':
      switch ($pane->subtype) {
        case 'node_body':
          $ak = array_keys($contexts);
          $content_type = $contexts[$ak[0]]->restrictions['type'][0];
          $doc = $content->content[0]['#markup'];
          $content->content[0]['#markup'] = img_capt_image_caption($doc, $content_type);
          break;
      }
      break;

    case 'entity_field':
      switch ($pane->subtype) {
        case 'node:body':
          $ak = array_keys($contexts);
          $language = $contexts[$ak[0]]->data->language;
          $content_type = $contexts[$ak[0]]->restrictions['type'][0];
          $doc = $contexts[$ak[0]]->data->body[$language][0]['value'];
          $content->content[0]['#markup'] = img_capt_image_caption($doc, $content_type);
          break;
      }
      break;

    case 'node_content':
      switch ($pane->subtype) {
        case 'node_content':
          $ak = array_keys($contexts);
          $language = $contexts[$ak[0]]->data->language;
          $content_type = $contexts[$ak[0]]->restrictions['type'][0];
          if (isset($content->content['body'])) {
            $doc = $content->content['body'][0]['#markup'];
            $content->content['body'][0]['#markup'] = img_capt_image_caption($doc, $content_type);
          }
          break;
      }
      break;

    case 'custom':
      switch ($pane->subtype) {
        case 'custom':
          // Is this pane in settings?
          if (isset($pane->configuration['admin_title'])) {
            foreach ($settings as $set_name => $setting) {
              if (in_array($pane->configuration['admin_title'], $setting)) {
                $doc = $content->content;
                $content->content = img_capt_image_caption($doc, 'custom', $pane->configuration['admin_title']);
                break;
              }
            }
          }
          break;

        default:
          // Pane admin title does not exist, could it bee that this is
          // custom content that is linked?
          foreach ($settings as $setting) {
            if ($setting['pane_admin_title'] == $pane->subtype) {
              $doc = $content->content;
              $content->content = img_capt_image_caption($doc, 'custom', $pane->subtype);
            }
          }
      }
      break;

    case 'panels_mini':
      foreach ($settings as $setting) {
        if ($setting['pane_admin_title'] == $pane->subtype) {
          $doc = $content->content;
          dpm($doc, "Document");
          $content->content = img_capt_image_caption($doc, 'custom', $pane->subtype);
        }
      }
      break;

  }
}

