<?php

/**
 * Just a helper function.
 *
 * @param string $doc
 *   Partial HTML document eventually containing image(s).
 * @param string $nodetype
 *   Type of node to take into consideration.
 * @param string $pane_pane_admin_title
 *   Admin title in the pane.
 */
function img_capt_image_caption($doc, $nodetype, $pane_pane_admin_title = '') {
  // Variable variables used here, see install-file for more info.
  // $settings = unserialize(base64_decode(variable_get('img_capt_settings')));
  $settings = img_capt_get_sanetized_settings();

  // Is requested content type in settings?
  $is_in_settings = FALSE;
  foreach ($settings as $set_name => $set_arr) {
    if ($set_arr['content_type'] == $nodetype) {
      $is_in_settings = TRUE;
    }
  }
  if ($is_in_settings == FALSE) {
    return $doc;
  }

  $go_on = FALSE;
  $library = libraries_load('simple_html_dom');
  if ($library && !empty($library['loaded'])) {
    $go_on = TRUE;
  }

  if (!$go_on) {
    drupal_set_message($library['error message'], 'error');
    drupal_goto($GLOBALS['base_url'] . '/admin/config/media/img_capt');
    drupal_exit();
  }

  $html = new simple_html_dom();
  $html = $html->load($doc);

  $body = $doc;
  $path = realpath('.');
  $show_im_size_warn = FALSE;

  $active_setting = 'Default';

  // Checking against all setting except default.
  $trav_set = $settings;
  // Q: is this /dev/null?
  array_shift($trav_set);

  // Now it is time to check the settings if content_path contains a wildcard.
  $match_found = FALSE;
  if (count($trav_set) > 0) {
    foreach ($settings as $set_name => $setting) {
      // Check for "*" in content path.
      if (strpos($setting['content_path'], '*') === FALSE) {
        // Did not find a "*".
        // Is this the correct path, content type and pane admin title?
        // Is this the correct path, content type and pane admin title?
        if ( ($setting['content_path'] == request_path()) &&
             ($setting['content_type'] == $nodetype) &&
             ($setting['pane_admin_title'] == $pane_pane_admin_title)
           ) {
          $active_setting = $set_name;
          $match_found = TRUE;
          break;
        }
      } else {
        // We have a "*" setting.
        if (strlen($setting['content_path']) == 1) {
          // We have a single star: *
          if ( ($setting['content_type'] == $nodetype) &&
               ($setting['pane_admin_title'] == $pane_pane_admin_title)
             ) {
            $active_setting = $set_name;
            $match_found = TRUE;
            break;
          }
        }
        // We have some more characters.
        $wild_path = str_replace('*', '(.*?)', $setting['content_path']);
        $wild_path = str_replace('|', '\|', $wild_path);
        $content_path_match = preg_match('|' . $wild_path . '|', request_path());
        if (
             $content_path_match &&
             ($nodetype == $setting['content_type']) &&
             ($setting['pane_admin_title'] == $pane_pane_admin_title)
           ) {
          $active_setting = $set_name;
          $match_found = TRUE;
          break;
        }
      }
    }
  }

  // Initialize vars.
  $div_pre = '';
  $div_pre_style = '';
  $div_aft = '';
  $cap_tag = '';
  $cap_tag_style = '';
  $def_caption = '';
  $caption_markup = '';
  $caption_count = '';
  /* Yes, they are used.
  $content_path = '';
  $pane_admin_title = '';
  $content_type = '';
  $weight = '';
  */

  // Load variable variables here as per
  // http://php.net/manual/en/language.variables.variable.php
  // use settings for the current page if it exists.
  foreach ($settings[$active_setting] as $key => $value) {
    $$key = $value;
  }

  // Check pane admin title.
  if ((strlen($pane_pane_admin_title) > 0) && ($nodetype == 'custom')) {
    $go_on = TRUE;
  }

  if (!$go_on) {
    return $doc;
  }

  drupal_add_css(drupal_get_path('module', 'img_capt') . '/css/caption.css');
  $css = file_get_contents(realpath('.') . '/' . drupal_get_path('module', 'img_capt') . '/css/caption.css');
  preg_match('/' . preg_quote('.img-capt-caption-wrapper') . '(.*?)padding\:.?(\d{1,3})px/s', $css, $css_matches);
  if (isset($css_matches[2])) {
    $wrapper_padding = $css_matches[2];
  }
  else {
    $wrapper_padding = 0;
  }

  $i = 0;
  $images = array();
  while (($image = $html->find('img', $i++)) != NULL) {
    $image_orig   = $image->outertext;
    $id_attr      = $image->id;
    $style_attr   = $image->style;
    $alt_attr     = $image->alt;
    $title_attr   = $image->title;
    $width_attr   = $image->width;
    $height_attr  = $image->height;
    $size_attr    = $image->get_display_size();
    $align_attr   = $image->align;
    $rel_attr     = $image->rel;
    $image->removeAttribute('id');
    $image->removeAttribute('alt');
    $image->removeAttribute('title');
    $image->removeAttribute('style');
    // $image->removeAttribute('width');
    $image->width = "100%";
    $image->removeAttribute('height');
    $image->removeAttribute('align');
    $im_text = $image->outertext;

    $images[$im_text]['id'] = $id_attr;
    $images[$im_text]['style'] = explode(";", $style_attr);
    $images[$im_text]['alt'] = $alt_attr;
    $images[$im_text]['title'] = $title_attr;
    $images[$im_text]['src'] = $image->src;
    $images[$im_text]['width'] = $width_attr;
    $images[$im_text]['height'] = $height_attr;
    $images[$im_text]['size'] = $size_attr;
    $images[$im_text]['wrapper_style'] = '';
    $images[$im_text]['new_img'] = $im_text;
    $images[$im_text]['align'] = $align_attr;

    if ($rel_attr != "nocaption") {
      // Process alt-title attributes.
      // Empty string.
      $alt_empty = ((strlen($images[$im_text]['alt']) == 0) ? TRUE : FALSE);

      // Not set.
      $alt_noset = (($images[$im_text]['alt'] == FALSE) ? TRUE : FALSE);
      $alt_attr = $alt_empty || $alt_noset;

      // Empty string.
      $title_empty = ((strlen($images[$im_text]['title']) == 0) ? TRUE : FALSE);

      // Not set.
      $title_noset = (($images[$im_text]['title'] == FALSE) ? TRUE : FALSE);
      $title_attr = $title_empty || $title_noset;
      $def_caption .= ' ';
      if ($alt_attr && $title_attr) {
        $images[$im_text]['alt'] = $def_caption . $caption_count;
        $images[$im_text]['title'] = $def_caption . $caption_count;
        ++$caption_count;
      }
/*
      // Force using caption if either ALT or TITLE is set.
      if (!$alt_attr && $title_attr) {
        $images[$im_text]['title'] = $images[$im_text]['alt'];
      }
      if ($alt_attr && !$title_attr) {
        $images[$im_text]['alt'] = $images[$im_text]['title'];
      }
*/
      // Check to see if it is "ArtikelBilder"
      // If so: prepend "&copy;xxx"
      $prep_txt = '';
      $temp = explode("/", $images[$im_text]['src']);
      if (in_array('ArtikelBilder', $temp)) {
        foreach ($temp as $k => $v) {
          if ($v == 'ArtikelBilder') {
            $prep_txt = '&copy;&nbsp;' . $temp[$k + 1] . "<br/>";
            break;
          }
        }
        // Get the file name.
        $file_name = array_pop($temp);
        // Check if the name starts with "small_", "medium_" or "large_"
        $parts = explode("_", $file_name);
        if (count($parts) > 1) {
          // We have a split
          if (in_array($parts[0], array('small', 'medium', 'large'))) {
            // Yes - the file name is a miniature
            $file_name = str_replace($parts[0] . '_', "", $file_name);
          }
        }
        // Now get the alt & title tags from DB.
        $query = db_select('field_data_field_gallery_image', 'f_image');
        $query->fields('f_image', array('field_gallery_image_alt', 'field_gallery_image_title'));
        $query->leftjoin('file_managed', 'f_m', 'f_image.field_gallery_image_fid = f_m.fid');
        $query->condition('f_m.filename', $file_name, '=');
/*
          select
          field_gallery_image_alt,
          field_gallery_image_title
          from field_data_field_gallery_image
          left join file_managed
            on field_data_field_gallery_image.field_gallery_image_fid = file_managed.fid
          where file_managed.filename like '61.jpg';
*/
        $result = $query->execute();
        foreach ($result as $record) {
          $images[$im_text]['alt'] = $record->field_gallery_image_alt;
          $images[$im_text]['title'] = $record->field_gallery_image_title;
        }
      }

      $image->setAttribute('alt', $prep_txt . $images[$im_text]['alt']);
      $image->setAttribute('title', $prep_txt . $images[$im_text]['title']);
      $images[$im_text][$caption_markup] = $image->getAttribute($caption_markup);

      // Get image size if not defined and GD is installed.
      if ($images[$im_text]['size']['width'] == -1) {
        if (extension_loaded('gd') && function_exists('imagesx')) {
          $img_path = "";
          if (substr($images[$im_text]['src'], 0, 4) == 'http') {
            // This is an external image - hotlinking are we?
            $img_path = $images[$im_text]['src'];
            // $img = imagecreatefromstring(file_get_contents());
          } else {
            // Internal image.
            $img_path = $path . $images[$im_text]['src'];
            // $img = imagecreatefromstring(file_get_contents());
          }
          $file_content = @file_get_contents($img_path);
          if ($file_content === FALSE) {
            $img = imagecreate(1, 1);
          } else {
            $img = imagecreatefromstring($file_content);
          }
          $images[$im_text]['size']['width'] = imagesx($img);
          $images[$im_text]['size']['height'] = imagesy($img);
          imagedestroy($img);
        }
        else {
          $show_im_size_warn = TRUE;
        }
      }

      foreach ($images[$im_text]['style'] as $key => $value) {
        $temp = explode(":", $value);
        if (in_array(
          strtolower(trim(
            $temp[0])
          ), array(
            'float',
            'margin',
            'margin-top',
            'margin-right',
            'margin-bottom',
            'margin-left'))) {
          $images[$im_text]['wrapper_style'][] = $value;
          unset($images[$im_text]['style'][$key]);
        }
      }

      $images[$im_text]['wrapper_style'][] = 'max-width: ' .
        ($images[$im_text]['size']['width'] + $wrapper_padding * 2) . 'px';
      $images[$im_text]['wrapper_style'][] = 'width: ' .
        ($images[$im_text]['size']['width'] + $wrapper_padding * 2) . 'px';
      if ($align_attr != "") {
        $images[$im_text]['wrapper_style'][] = 'float: ' . $align_attr;
      }
      $images[$im_text]['wrapper_style'][] = $div_pre_style;

      $images[$im_text]['style'] = trim(implode(";", $images[$im_text]['style']));
      $images[$im_text]['wrapper_style'] = implode(
        ";", $images[$im_text]['wrapper_style']);

      $image->setAttribute('style', $images[$im_text]['style']);

      $replace_with  = $div_pre . ' style="' .
        $images[$im_text]['wrapper_style'] . '" >';
      $replace_with .= $image->outertext . '<' .
        $cap_tag . ' class="img-capt-caption" style="' . $cap_tag_style . '">';
      $replace_with .= $images[$im_text][strtolower($caption_markup)] .
        '</' . $cap_tag . '>' . $div_aft;

      $body = preg_replace('|' . preg_quote($image_orig) . '|', $replace_with, $body);

    } // End rel_attr
  } // End while.

  $html->clear();
  unset($html);

  if ($show_im_size_warn) {
    drupal_set_message(t('You should specify image size. PHP GD is not installed and I can not calculate image size.'), 'warning');
  }

  return $body;
}

