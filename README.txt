Introduction
------------
Wraps all images in content with a <DIV> tag, ALT attribute is used to
set he caption text. If the ALT attribute is empty then the TITLE attribute
is used as a copy to ALT attribute and caption. If both attributes are empty
use a template string for caption with a sequential numbering. It does all
its work on the server.

Wrapping occurs prior Drupal renders the node content. Original content is
replaced with the new content only before rendering, thus the new content is
NOT saved anywhere. Ok, maybe in Drupals cached files but not anywhere else.

Works with CKEditor, Panels, Panels Everywhere. In CKEditor you just enter
the caption string in the text field "Alternative Text".

Based on idea of
----------------
This module is inspired by
Image Caption, https://www.drupal.org/project/image_caption
Caption Filter, https://www.drupal.org/project/caption_filter
It does its work without any Javascript on the client side.

Help
----
See Configuration->Media->Image Caption & More (admin/config/media/img_capt)

Drush
-----
Drush is supported with the following commands:
  drush help img-capt
    Show img-capt help.
  drush img-capt
    Shows comprehensive overview of existing configurations.
  drush img-capt <configuration-name>
    Shows detailed view about the configuration.
  drush img-capt <configuration-name> <setting-name>
    Show just the setting setting-name of configuration configuration-name.
  drush img-capt <configuration-name> <setting-name> <setting-value>
    Set setting-name to setting-value in configuraiont configuration-name.
    Note: Since all arguments that are passed to drush will have a preceding
          dash (-) you can not pass negative values (e.i. -100) to img-capt
          command. Drush will abort and give you one - or more - errors.
          To pass a negative value replace the dash with underscore,
          thus -100 becomes _100.
  drush img-capt create <configuration-name>
    Create (copy Default settings to) configuration configuration-name.
    Note: If configuration-name exists its values will be overwritten with
          settings from Default.
  drush img-capt delete <configuration-name>
    Delete configuration configuration-name.
    Note: The Default configuration can not be deleted.

Please note since all arguments after img-capt are strings and case sensitive
surround them with a quote, " or '.
This will work:
  drush img-capt Default
  drush img-capt "Default"
  drush img-capt 'Default'
  drush img-capt "Default config"
  drush img-capt 'Default config'

External library
----------------
External library used by this module: PHP Simple HTML DOM Parser
Download: http://sourceforge.net/projects/simplehtmldom/files/
Version: 1.5
Place in: sites/all/libraries/simple_html_dom/

Install
-------
See https://www.drupal.org/documentation/install/modules-themes/modules-7

License
-------
GNU General Public License, version 2 or greater; the same as Drupal itself.

License of image gear.png
-------------------------
The image is Public Domain.

As per http://openclipart.org/share follows quote:
"Unlimited Commercial Use
We try to make it clear that you may use all clipart from Openclipart
even for unlimited commercial use. We believe that giving away our images
is a great way to share with the world our talents and that will come
back around in a better form.

May I Use Openclipart for?
We put together a small chart of as many possibilities and questions
we have heard from people asking how they may use Openclipart. If you
have an additional question, please email love@openclipart.org.
All Clipart are Released into the Public Domain.

Each artist at Openclipart releases all rights to the images they
share at Openclipart. The reason is so that there is no friction in
using and sharing images authors make available at this website so
that each artist might also receive the same benefit in using other
artists clipart totally for any possible reason."
