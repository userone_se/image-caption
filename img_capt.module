<?php

/**
 * @file
 * Wrap every HTML IMG tag with content of supplied IMG tag attribute.
 */


/**
 * Definitions:
 * MODULE-NAME_STRUCTURED-DATA
 */
define('IMG_CAPT_PATH', DRUPAL_ROOT . '/' . drupal_get_path('module', 'img_capt'));
define('IMG_CAPT_PATH_INCLUDE', IMG_CAPT_PATH . '/include/');
define('IMG_CAPT_PATH_DOCS', IMG_CAPT_PATH . '/docs/');

/**
 * Implements hook_menu().
 */
function img_capt_menu() {
  $items = array();

  $items['admin/config/media/img_capt'] = array(
    'title'             => 'Image Caption & More',
    'description'       => 'Help and description',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('img_capt_help_desc'),
    'access arguments'  => array('administer img_capt'),
    'access callback'   => TRUE,
    'type'              => MENU_NORMAL_ITEM,
  );

  $items['admin/config/media/img_capt/admin'] = array(
    'title'             => 'Administer ICM',
    'description'       => 'Settings for Image Caption & More',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('img_capt_ajax_load_setting'),
    'access arguments'  => array('administer img_capt'),
    'access callback'   => TRUE,
    'type'              => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_help_desc().
 */
function img_capt_help_desc() {
  $path = drupal_get_path('module', 'img_capt');
  drupal_add_css($path . '/css/admin-page.css');
  drupal_add_css($path . '/css/caption.css');
  $help_file = file_get_contents(IMG_CAPT_PATH_DOCS . 'README.html');
  $help_file = preg_replace('/\{path\}/', $path, $help_file);
  $help_file = preg_replace('/\{base_url\}/', $GLOBALS['base_url'], $help_file);

  $form = array();

  $form['help_description'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'help-description',
    ),
  );

  $form['help_description']['content'] = array(
    '#type' => 'item',
    '#markup' => $help_file,
  );

  return $form;
}

/**
 * Load setting through ajax and return a settings.
 */
include_once(IMG_CAPT_PATH_INCLUDE . 'img_capt_ajax_load_setting.php.inc');

/**
 * Callback for AJAX when user selects one active setting.
 */
function img_capt_ajax_load_setting_callback($form, $form_state) {
  return $form['form_settings']['settings'];
}

/**
 * Implements hook_admin_validate().
 */
include_once(IMG_CAPT_PATH_INCLUDE . 'img_capt_admin_validate.php.inc');

/**
 * Implements hook_permission().
 */
function img_capt_permission() {
  return array(
    'administer img_capt' => array(
      'title' => 'Administer Image Caption & More',
      'description' => t('Perform administration tasks for Image Caption & More'),
    ),
  );
}

/**
 * Implements hook_panels_pane_content_alter().
 *
 * This one is for Drupal with Panels/Panes Everywhere.
 */
include_once(IMG_CAPT_PATH_INCLUDE . 'img_capt_panels_pane_content_alter.php.inc');

/**
 * Implements hook_node_view().
 *
 * This one is for Drupal with NO Panels/Panels Everywhere
 */
include_once(IMG_CAPT_PATH_INCLUDE . 'img_capt_node_view.php.inc');

/**
 * Get (sanetized) settings.
 *
 * @param bool $is_drush
 *   Set by drush to avoid going to the admin page.
 */
include_once(IMG_CAPT_PATH_INCLUDE . 'img_capt_get_sanetized_settings.php.inc');

/**
 * Just a helper function.
 *
 * @param string $doc
 *   Partial HTML document eventually containing image(s).
 * @param string $nodetype
 *   Type of node to take into consideration.
 * @param string $pane_pane_admin_title
 *   Admin title in the pane.
 */
include_once(IMG_CAPT_PATH_INCLUDE . 'img_capt_image_caption.php.inc');

/**
 * Implements hook_library_info().
 */
include_once(IMG_CAPT_PATH_INCLUDE . 'img_capt_libraries_info.php.inc');

